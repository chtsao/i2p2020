---
title: "About"
date: 2020-09-12T07:03:02+08:00
draft: false
---
![Dice](https://meeplelikeus.b-cdn.net/wp-content/uploads/2018/03/cameringo_20180225_092747.jpg)


* [Syllabus](http://faculty.ndhu.edu.tw/~chtsao/edu/20/i2p/i2p2020syllabus.pdf)    Curator: C. Andy Tsao  Office: SE A411.  Tel: 3520
* Lectures: Tue. 1310-1500, Thr. 1610-1700 @ A210
Office Hours:  MON 14:10-15:00；THU 15:10-16:00 @ SE A411 or by appointment.
* TA Office Hours (NEW)

  * 陳學莆：Mon. 0900-1000; Fri. 0900-1000 @ A412
  * 劉映彤：Fri: 1000-1200 @ A408
* Prerequisites: Calculus
* Textbook:  Dekking, Kraaikamp, Lopuhaä and Meester (2005). A Modern Introduction to Probability and Statistics: Understanding Why and How. Springer, London. [Legal downloadable from NDHU](http://134.208.29.176:8080/toread/opac/bibliographic_view?NewBookMode=false&id=766570&q=Modern+Introduction+to+Probability+and+STatistics&start=0&view=CONTENT)

