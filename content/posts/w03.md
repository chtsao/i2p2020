---
title: "Week 3. Conditional Probability"
date: 2020-09-30T03:52:10+08:00
draft: false
tags: [conditional probability, independence, Bayes theorem, mulltiplication rule, law of total probability]
---
![輝夜姬想讓人告白](https://p2.bahamut.com.tw/B/ACG/c/65/0000108265.JPG)
### Conditional probability
* $ P(A|B)  = P(A \cap B)/P(B)$ if $P(B) \neq 0.$
* [Prosecutor's fallacy](https://towardsdatascience.com/the-prosecutors-fallacy-cb0da4e9c039). 
P(Guilty|Evidence) $\neq$ P(Evidence|Guilty)   
可視為一個方便理解的數學呈現。

### Multiplication rule
* $P(A \cap B) = P(A|B) P(B)$
* In general, let $A_1, \cdots, A_n$ be n events, then 
$ P(A_1 \cap \cdots, A_n) = P(A_n|A_{n-1}\cap \cdots A_1) P(A_{n-1}|A_{n-2}\cap \cdots A_1) \cdots P(A_2|A_1) P(A_1)$
* [Birthday problem](https://en.wikipedia.org/wiki/Birthday_problem)
* [千分之一約等於十分之七的二十次方](https://youtu.be/1Nub7szsLow) (影片中的計算式有些小錯誤，你能抓出來嗎？)

### Law of Total Probabilities
* Let $\mathcal{C}= \{ C_1, \cdots, C_m \}$ be a partition then for any $A \subset \Omega$
$$ P(A) = \sum_{i=1}^m P(A|C_i) P(C_i)$$
* [Stratified Sampling](https://en.wikipedia.org/wiki/Stratified_sampling) [分層抽樣](https://zh.wikipedia.org/wiki/%E5%88%86%E5%B1%82%E6%8A%BD%E6%A0%B7) 

### Bayes Theorem
 * P(F|D) vs. P(F). .... To 告白 or not-- that is the question.
 * P(D|+) vs. P(+|D)
 * P(G|M) vs. P(M|G)

### Homework 2
* Sec 3.6 (Page 37): 3.1, 3.4, 3.10, 3.11, 3.12, 3.14  (to be discussed in class on 10/6)
* 三五好友一起討論既可分擔也更有效率，鼓勵小組戰隊！



