---
title: "Week 7.0. Jump Start: Help, Service and Go!"
date: 2020-10-26T20:40:00+08:00
draft: false
categories: [adm]
---
![](https://images.firstpost.com/wp-content/uploads/2019/08/redbull-rtr-1024.jpg)
## Kno and TA's

* Kno's Office Hours:  Mon. 14:10-15:00；Thr. 15:10-16:00 @ SE A411 or by appointment.

* TA Office Hours (NEW)
  * 陳學莆：Mon. 0900-1000; Fri. 0900-1000 @ A412
  * 劉映彤：Fri: 1000-1200 @ A408

## [數學小天地](http://faculty.ndhu.edu.tw/~yltseng/edu/help.html)

## 同儕輔導
* 守護天使：曾柏恩、陳慬瑜
* 時間/地點：Fri. 15：30~16：30 @ 數學小天地A320
