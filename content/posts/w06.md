---
title: "Week 6. Continuous Random Variable I "
date: 2020-10-23T11:36:22+08:00
draft: false
tags: [pdf, cdf, continous r.v.]
---
<img src="https://d279m997dpfwgl.cloudfront.net/wp/2018/02/0207_escher-01.jpg" style="zoom:67%;" />

### 破壞性創新：從 DU(K) 到 U(0,1)
1. pmf f(x) = P(X=x) 有單點機率解釋但限制在無限多點（特別是在區間等不可數無限多點）的應用。
2. 抉擇：丟掉一些（單點機率解釋：不見得是不好的，但也不得不），守住基本（刻劃該隨機變數可能出現的範圍，可計算在區間及其延伸的機率，量化） ==> pdf


### Continuous r.v.

A continuous random variable can be characterized by its pdf (probability density function), a function $f: \mathcal{R} \rightarrow \mathcal{R}$ and satisfies
1. $f(x) \geq 0$ for all $x \in \mathcal{R}$
2. $\int_{-\infty}^\infty f(x) dx =1$
and for any $a \leq b$ the probability 
$$ P(a \leq X \leq b) = \int_a^b f(x) dx. $$

Another characterization of $X$ is its cumulative distribution function (cdf or df) defined by 
$$ F(x) = P(X \leq x) = \int_{-\infty}^x f(t)dt $$
and for any $x$ 
$$ F'(x) = f(x)$$

### Compare and Contrast: pmf vs. pdf
簡單來說，pmf 有機率解釋；pdf 可以用來算機率，但本身並沒有機率解釋。

1. If $X$ is a discrete random variable with pmf $f$ and cdf $F$
then 
	* $F(x) = P(X \leq x).$ $\sum_{x: f(x)>0} f(x) =1.$
	* $0 \leq f(x) = P(X=x) \leq 1$ for any $x$.
2.  If $X$ is a continuous random variable with pdf $f$ and cdf $F$
then 
	* $F(x) = P(X \leq x).$ $\int_{-\infty}^\infty f(x) =1.$
	*  $f(x) \neq P(X=x)$ for some $x$, $P(X=x)=0$ for any $x$ and  $f(x)$ may be greater than $1$ for some $x$. 

#### 你可以學得更好, 更開心, 更有效率 
其他科目都可以不及格，只有學習如何學習絕對要學會。在現在這個時代更是如此。但與其說它是一個要修的科目，不如把它想成一種探索未知世界的態度。以下是最近看到一篇寫得相當好而精簡的部落格文。探討學習有不少經典的書籍。可貴的是，這篇文章精要地總結了許多現代關於學習的新理論與角度。以Kno~~一個大學成績不太好，但後來還學得蠻開心，在這領域多年依然樂在其中＋自學一些有的沒有的還能自得其樂的人~~的視角來看, 要是能早點在大學知道這些，應該可以學得更好，更開心也更有效率。我強烈推薦與分享

[The only technique to learn something new](https://boingboing.net/2015/05/11/the-only-technique-to-learn-so.html) ([James Altucher](https://boingboing.net/2016/04/15/how-minimalism-brought-me-free.html) @Boing Boing)

**學會任何新事物的唯一技法**。作者將它分為十個步驟，由愛她開始。文不算短，你可以先瀏覽大標題：1. Love It, 2. Read it. 3. Try it, but not too hard, ... 你沒看錯，由愛她開始。

加映一個相關的 quote
> Wisdom is a love affair with questions. Knowledge is a love affair with answers. 
> -- <cite>Julio Olalla</cite>