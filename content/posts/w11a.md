---
title: "Week 11.2. Moment Generating Function"
date: 2020-11-28T13:45:42+08:00
draft: false
tags: [moment, moment generating function, characterization]
---
![](https://miro.medium.com/max/573/1*3x5SIG4T9dD0G_PfgT6iig.jpeg)
Image by Gerd Altmann from Pixbay

## Identification of a r.v.

我們如何認出一個隨機變數---換個角度來說，一個隨機變數的 身份證 ID 是什麼？ 已討論過的 ID 有

* pmf for a discrete random variable (or pdf for a continuous random variable)
* distribution function, df,  (also known as cumulative distribution function, cdf )

另外一個重要的 ID 就是 moment generating function (mgf)

Let $X$ be a random variable then its mgf is defined as 

$M(t) =  E(e^{tX})$ if exists. 

Moment generating function 有兩個重要用途

1. 小用：計算動差. For $r \in \mathcal{N}$, the $r$-th moment of $X$ is  $E(X^r)$ if exists. It can be computed through mgf. 

    $$ E(X^r) = M^{(r)}(0)$$ if exists. 

2. 大用：認出 Identify a random variable.