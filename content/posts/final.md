---
title: "Final Exam"
date: 2021-01-08T05:20:57+08:00
draft: false
tags: [adm, final]
---
 <img src="https://static1.1.sqspcdn.com/static/f/669355/26725522/1449694822120/exams+ahead.JPG?token=7TtkmQcev3ldAIGzaKSaoSGcVHE%3D" alt="Final" style="zoom:67%;" />

@ [10 Ways to Infuse Your Final Exam Reviews With WICOR](https://avidcollegeready.org/college-career-readiness/2014/12/11/10-ways-to-infuse-your-final-exam-reviews-with-wicor.html)

### Final Exam

* 日期: 2021 0112 (二)
* 時間: 1310-1440. 
* 地點:  
  * A210: Student ID $> 410711330$
  * A307: Student ID $\leq 410711330$
* 範圍：上課及習題內容。對照課本章節約為: Chapter 5, Chapter 7; Chapter 8 (Sec 8.3, 8.4 除外); Chapter 9; Chapter 10; Chapter 11 (Sec 11.3 除外); Chapter 13. (Chapter 14 Central Limit Theorem 不考）
* 其他：No cheatsheet nor mobile phone. Prepare early and Good Luck!

提早準備，固實會的，加強生疏的，弄懂原來不會的！----考試不難，會就簡單！

### 考古題

I2P FINAL: , [2018etude](https://chtsao.gitlab.io/i2p2020/etude.pdf), [2018](https://chtsao.gitlab.io/i2p2020/fin19.pdf), [2019](https://chtsao.gitlab.io/i2p2020/fin20.pdf)

注意：今年內容包含了 continuous random vector 與過去幾年不同，因此沒有出現在過去考題中。但也是考試的重要主題，請自行針對課本習題內容練習。總之是要會，不要背。好好參考[Study less, study smart](https://chtsao.gitlab.io/preda2020/posts/w13/#study-less-study-smart)的幾個原則如 Quizz+Space+Mix. 加油！