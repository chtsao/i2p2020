---
title: "Week 7. Continuous Random Variables and Expectation"
date: 2020-10-29T04:06:36+08:00
draft: false
tags: [continuous random variable, profiling, expectation, Pareto principle]
---

![](https://ctrl-alt-success.com/wp-content/uploads/2019/01/pareto-principle-for-work-80-20-rule.png)

## Discrete vs. Continuous r.v.

1. What have we learned about discrete random variable?
2. Why bother introducing pdf rather than using pmf all the way?
3. Example: Discrete Uniform (K) vs. Uniform $[0,1]$

## Continuous Random Variables

* Uniform, denoted by $U(\alpha, \beta)$ where $\alpha < \beta$.
* Exponential, denoted by $Exp(\lambda)$ where $\lambda >0$
* Pareto, denoted by $Par(\alpha)$ where $\alpha >0$. [Pareto principle](https://management.simplicable.com/management/new/examples-of-the-pareto-principle).

You may use [WolframAlpha](https://www.wolframalpha.com/input/) to get the functional plots of these random variables. For example, 

* [Exponential distribution](https://www.wolframalpha.com/input/?i=exponential+distribution). [Pareto](https://www.wolframalpha.com/input/?i=Pareto+random+variable). 
* You may plot those discrete random variables as well, for example, [Bin(40,0.32)](https://www.wolframalpha.com/input/?i=binomial+distribution+n%3D40%2C+p%3D0.32)(binomial distribution n=40, p=0.32)

### Expectation

* Expectation as a fair price
* Expectation as a probability of a Bernoulli trial
* Definition and computation 
  * Compute $E(X)$ where $X \sim Bernoulli(p)$ and $X \sim Bin(n, p)$, respectively. Do it not just read it. 
  * Computing $E(X)$ where $X \sim  Bin(n, p)$ is a simple and good example of pattern recognition. Pattern recognition is one of the core  themes of math. 
* $E(g(X)).$